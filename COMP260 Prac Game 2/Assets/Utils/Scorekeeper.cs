﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour
{
    static private Scorekeeper instance;

    public int scorePerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;

    static public Scorekeeper Instance
    {
        get { return instance; }
    }

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError("More than one Scorekeeper exists in the scene.");
        }

        // reset the scores to zero
        for (int i = 0; i < 2; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

        scoreText[2].text = "Good Luck";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnScoreGoal(int player)
    {
        //Debug.LogError("player is " + player);

        score[player] += scorePerGoal;
        scoreText[player].text = score[player].ToString();

        if (score[0] == 4)
        {
            scoreText[2].text = "Player 0 wins!";
            Time.timeScale = 0;
        }
        else if (score[1] == 4)
        {
            scoreText[2].text = "Player 1 wins!";
            Time.timeScale = 0;
        }
    }

}
