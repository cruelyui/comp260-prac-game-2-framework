﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour
{
    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;

    private AudioSource audio;

    public Transform startingPos;
    private Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();

        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Collision Enter: " + collision.gameObject.name);
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
        }
    }

    void OnCollisionStay(Collision collision)
    {
        //Debug.Log("Collision Stay: " + collision.gameObject.name);
    }

    void OnCollisionExit(Collision collision)
    {
        //Debug.Log("Collision Exit: " + collision.gameObject.name);
    }

    public void ResetPosition()
    {
        // teleport to the starting position
        rigidbody.MovePosition(startingPos.position);

        // stop it from moving
        rigidbody.velocity = Vector3.zero;
    }

}
