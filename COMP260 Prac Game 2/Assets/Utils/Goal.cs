﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
//[RequireComponent(typeof(PuckControl))]
public class Goal : MonoBehaviour
{
    public AudioClip scoreClip;
    private AudioSource audio;

    //private PuckControl puck;

    public int player; // who gets points for scoring in this goal

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        //puck = FindObjectOfType<PuckControl>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        // play score sound
        audio.PlayOneShot(scoreClip);

        // tell the scorekeeper
        Scorekeeper.Instance.OnScoreGoal(player);

        // reset the puck to its starting position
        // reset the puck to its starting position
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();
    }

}
